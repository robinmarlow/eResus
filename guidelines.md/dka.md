<!--DKA-->

This is for *_emergency quick reference only_* - Always print a copy of [BSPED protocol](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#18097)

![DKA](./guidelines.md/dka_fig_1.svg#centre)

--- 
eResus v2.0 based on [WATCH DKA protocol v1.1](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#18097)