<!--Major Haemorrhage-->
![Major Haemorrhage protocol](./guidelines.md/majorHaemorrhage.png)

## Activating the major haemorrhage protocol
- Call 2222
- State “I would like to trigger the major haemorrhage procedure in (location), extension (xxxx)
- Switchboard will call blood bank and return your call
- Provide patient identification details, age, weight and products required 

##Definition of Major Haemorrhage
Suspected active haemorrhage plus	

- Bleeding rate > 2ml/kg/min
- 20 ml/kg PRC in preceding hour
- 40 ml/kg any fluid in preceding hour
- Hypovolaemic shock and/or coagulopathy

#Tranexamic Acid
__Early administration is vital for efficacy__
#### Loading dose
- 15mg/kg (max 1 gram)
- dilute in convenient volume of Sodium Chloride 0.9% or Glucose 5% 
- give over 10 minutes
#### Maintenance infusion
- 2mg/kg/hr
- make 500mg in 500ml of Sodium Chloride 0.9% or Glucose 5%
- give at 2mls/kg/hour for at least 8 hours or until bleeding stops.

--- 
eResus v2.0 based on  [Major Haemorrhage v 4.0](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#17077)