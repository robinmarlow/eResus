![Intubation checklist](./guidelines.md/rsiChecklist_fig_1.svg)

Also see: [difficult airway guidelines](#home&resus&difficultAirway.md)

--- 
eResus v2.0 based on [Intubation Checklist v5 (COVID)](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#24112)