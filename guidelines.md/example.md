<!--Example of how to write an eResus Page-->

Below is a picture,  you specify the title and then where it should look for the file.
This could be a png,  but I think svg is better as it's smaller.
![APLS algorithm](./apls_fig_1_flowchart.svg#centre)

Unfortunately they won't actually work in the web editor

different sized titles are
# like This
## like This
### like This
#### or this

Bullet points
- are done
- like so

Then to make it appear you need to include it in the file assets/data/eResus.json

--- 
eResus v2.0 based on [APLS 6th edition](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#18097)

[Resus council 2021 algorithm](https://www.resus.org.uk/sites/default/files/2021-04/Paediatric%20ALS%20Algorithm%202021.pdf)
ALSG 6e
[https://www.alsg.org/fileadmin/_temp_/Specific/Ch06_CA.pdf](https://www.alsg.org/en/files/Ch06_CA2006.pdf)
