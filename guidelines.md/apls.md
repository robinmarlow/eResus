<!--APLS Algorithm-->
![APLS algorithm](./guidelines.md/apls_fig_1_flowchart.svg#centre)

--- 
eResus v2.0 based on [APLS 6th edition](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#18097)

[Resus council 2021 algorithm](https://www.resus.org.uk/sites/default/files/2021-04/Paediatric%20ALS%20Algorithm%202021.pdf)
ALSG 6e
[https://www.alsg.org/fileadmin/_temp_/Specific/Ch06_CA.pdf](https://www.alsg.org/en/files/Ch06_CA2006.pdf)
