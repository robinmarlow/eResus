<!--Burns-->
![Burns algorithm](./guidelines.md/burns_fig_1.svg)

## BSA and Depth
![Burns algorithm](./guidelines.md/burns2.png)

Assessment of depth is difficult and not necessary for initial management (refer to full guidelines for depth assessment)

Calculate BSA using Lund Browder chart
or [Mersey burns app online](https://app.merseyburns.com/)

Do NOT include superficial burns (erythema only) in %TBSA calculation

## Fluids
Indications for resuscitation and maintenance fluids

\> 1 month scald ≥ 20% TBSA or flame ≥ 10% TBSA
Neonate (< 4 weeks) scald or flame ≥  5%

If >1 month and scald 10 – 19% TBSA do NOT give resuscitation fluids. Give 80% maintenance fluids (discuss route with burns team). 

### Resucitation fluids
   Total volume (Hartmanns)  =  weight (kg)  x  %TBSA  x  2

½ over first 8 hours from injury
½ over next 16 hours
Include fluid boluses already given
Delayed fluid resuscitation do not administer rapid catch up
Adjust according to clinical status. Target UO 0.5 – 1.0 ml/kg (if child well do not chase UO)

### Maintenance  Fluids
IV maintenance fluids only if not tolerating oral fluids or < 12 months
Give 80% maintenance fluids
1st 10kg = 4 ml/kg/hour
2nd 10kg = 2ml/kg/hour
Extra extra kg = 1ml/kg/hour

Check U&Es 8 – 12 hourly
If < 12 months check BM
Consider catheterization

--- 
eResus v2.0 based on  [Fluids management in burns V2.0](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#17056)