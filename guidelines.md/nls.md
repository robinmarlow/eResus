![NLS flowchart](./guidelines.md/nls_fig_1_flowchart.svg#centre)
--- 
eResus v2.0 based on [Resus council NLS 2021](https://www.resus.org.uk/sites/default/files/2021-05/Newborn%20Life%20Support%20Algorithm%202021.pdf)
