<!--Tachycardia-->
### Supra Ventricular Tachycardia
![SVT](./guidelines.md/tachycardia_fig_1_svt.svg)


--- 
### Ventricular Tachycardia
![VT](./guidelines.md/tachycardia_fig_2_vt.svg)


--- 
### Pulseless Ventricular Tachycardia
![Pulseless VT](./guidelines.md/tachycardia_fig_3_vt_pulseless.svg)


--- 
### Torsades de Points
![TDP](./guidelines.md/tachycardia_tdp.png)Treatment magnesium sulphate
Dose 25–50 mg/kg (max 2 g) rapid IV infusion over several minutes

--- 
eResus v2.0 based on [SVT V2](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#18664)
[WATCh management of tachyarrythmia](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#18664)