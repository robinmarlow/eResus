<!--Pelvic Fracture-->
![Pelvic Fracture](./guidelines.md/pelvicFracture_fig_1.svg#centre)
<br>

--- 
eResus v2.0 based on [Pelvic Fracture V1](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#17926)
