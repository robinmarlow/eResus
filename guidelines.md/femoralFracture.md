<!--Femoral Fracture-->
![Femoral](./guidelines.md/femoralFracture_fig_1.svg)

--- 
eResus v2.0 based on [Femoral shaft fractures v1.0](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#18159)