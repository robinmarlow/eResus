![Drug Doses](./guidelines.md/drugDoses_fig_1.svg#centre)
--- 
eResus v2.0 based on [Resus council emergency drug doses 2021](https://www.resus.org.uk/sites/default/files/2021-05/2492%20AAP%20RCUK%20PET%20chart-5.pdf)