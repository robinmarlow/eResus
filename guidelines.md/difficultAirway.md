[Difficult Mask Ventilation](#dmv) / [Unanticipated difficult intubation](#uda) / [CICV](#cicv)
![Difficult Mask Ventilation](./guidelines.md/difficultAirway_fig_1_dmv.svg#centre)
---
![Unanticipated difficult intubation](./guidelines.md/difficultAirway_fig_2_uda.svg#centre)
---
![Can't intubate can't ventilate](./guidelines.md/difficultAirway_fig_3_cicv.svg#centre)
--- 
eResus v2.0 based on [Difficult airway society](https://das.uk.com/guidelines/paediatric-difficult-airway-guidelines)



