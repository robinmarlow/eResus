<!--Septic Shock-->
![Septic Shock](./guidelines.md/septicShock_fig_1.svg#centre)
--- 
eResus v2.0 based on [Sepsis in the ED v3](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#20191)
