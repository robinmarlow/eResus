<!--hypercyanoticSpells-->
![Hypercyanotic](./guidelines.md/hypercyanoticSpells_fig_1.svg#centre)
--- 
eResus v2.0 based on
[Hypercyanotic spell management V2.1](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#8499)
