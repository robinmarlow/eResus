<!--Anaphylaxis-->
![flowchart](./guidelines.md/anaphylaxis_fig_1.svg#centre)
---
<br>
###Drug Doses
<br>
![treatment](./guidelines.md/anaphylaxis_fig_2.svg#centre)
--- 
<br>
###See full guideline for Glucagon  / adrenaline infusion details

eResus v2.0 based on [Anaphylaxis v3.1](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#14843)