<!--Hypothermic arrest-->
![Hypothermic arrest](./guidelines.md/hypothermic_arrest_fig_1.svg#centre)
<br>
---
### Why is defibrillation limited to 3 shocks at temperatures less than 30&deg;C?
Arrhythmias are more common but may be refractory at lower temperatures and therefore is limited to a total of 3 shocks.
Adrenaline and amiodarone should not be given <30&deg;C

---
###What does doubling the dose interval mean?
####Shockable rhythms
In shockable rhythms without hypothermia, adrenaline is given after the 3rd and 5th shock.
In shockable rhythms with hypothermia between 30-35 &deg;C, adrenaline should be given after the 3rd/6th/9th shock.
####Non Shockable rhythms
In non shockable rhythms without hypothermia adrenaline is given straight away and every 4 minutes.
In non shockable rhythms with hypothermia between 30-35 degrees, adrenaline should be given straight away and every 8 -10 minutes.

---
eResus v2.0 based on [APLS 6th edition](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#18097)
[Drowning v2.0](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#18983)