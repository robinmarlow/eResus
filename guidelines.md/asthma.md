<!--Asthma-->
![Asthma protocol](./guidelines.md/asthma_fig_1_flowchart.svg#centre)

##Formulary

### Magnesium sulphate
Make up a 100 mg/ml solution by diluting 2 grams of magnesium sulphate in 20 mls of 0.9% sodium chloride (draw up 4 mls of the 5 grams in 10 mls solution of magnesium sulphate and add to 16 mls of 0.9% sodium chloride).
- Ensure that ECG and saturation monitoring are in place
- Infuse 40 mg/kg (0.4 mls/kg) via a syringe driver over 20 minutes (but can be given as a slow push in life-threatening asthma)
- Maximum dose is 2 grams or 20 mls of the 100 mg/ml solution.

---

### Peripheral Intravenous Salbutamol

- All children requiring IV salbutamol should be reviewed by an experienced doctor
- Patients should receive continuous ECG monitoring to ensure any arrhythmias are detected, and should also have saturation monitoring in place
- IV salbutamol may cause hypokalaemia therefore measure electrolytes twice daily
- IV salbutamol is not compatible with IV aminophylline. IV salbutamol is Y-site compatible with potassium infusions, only if mixed with sodium chloride 0.9%
- Once made, infusion bags are stable for 24 hours
- Inform both HDU and the hospital outreach team
For patients with central access see the PICU asthma guideline and drug sheet

####Salbutamol loading dose and Infusion (for use in severe and life-threatening asthma)
This guideline does not advocate the use of a salbutamol bolus as suggested in the Children’s BNF (5 microgram/kg or 15 microgram/kg) as local audit data has shown that this rarely stops and often delays a child going on to receive a loading dose or infusion. However, experienced consultants may still recommend a salbutamol bolus in individual cases (e.g. cases of moderate asthma slow to respond to inhaled bronchodilator therapy).

__For both loading doses and infusions:__
*Children under 40 kg (under 11 years based upon APLS weight calculation)*
Dilute 10 mg of intravenous salbutamol (2 vials of the 5 mg in 5 ml strength) with 40 mls of 0.9% sodium chloride to give
a solution of 10 mg in 50 mls (200 microgram/ml)

*Children over 40 kg (11 years or older based upon APLS weight calculation)*
Dilute 100 mg of intravenous salbutamol (20 vials of the 5 mg in 5 ml strength) with 400 mls of 0.9% sodium chloride
to give a solution of 100 mg in 500 mls (200 microgram/ml)

__Loading dose:__ Run the 200 microgram/ml strength solution at 5 micrograms/kg/min (1.5 ml/kg/hour) for 1 hour.
*Ensure ECG monitoring and consider reducing loading dose rate if extreme tachycardia.*
__Infusion:__ After the loading dose reduce the infusion rate to 1 -2 micrograms/kg/min (0.3-0.6ml/kg/hour).
PICU admission should be considered in any child requiring an infusion of \>1microgram/kg/minute.

---
### Intravenous Aminophyline
Dilute 500mg of IV aminophylline (2 vials of the 250 mg in 10 ml strength) with 480 mls 0.9% sodium chloride to give a solution of 1 mg/ml.

__Loading dose:__
- 5 mg/kg (5 ml/kg) over 20 minutes (If weight over 66 kg then loading dose should be given over 30 minutes)
- Maximum dose is 500mg (500ml)
- Ensure that ECG and saturation monitoring are in place

*It is important to ensure that a volume limit is set on the infusion pump so that no more than the 5 mg/kg bolus is given before changing to the continuous infusion*

__Infusion:__
- Children under 40 kg: 1 mg/kg/hour (1 ml/kg/hour)
- Children over 40 kg: 0.5-1 mg/kg/hour (0.5-1ml/kg/hour)

---

## Rapid Sequence Induction in Asthma 

__Experienced clinical only: GET HELP__

- Atropine 20 micrograms/kg
- Ketamine 1 – 3 mg/kg
- Suxamethonium 1 – 2 mg/kg

Plus fluid bolus (20ml/kg 0.9% sodium chloride) and resuscitation dose of adrenaline

## Other relevant guidelines
[RSI checklist](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#24112)
[WATCh asthma guidance](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#25225)

--- 
eResus v2.0 based on [Asthma v5.0](http://workspaces/sites/Teams/ChildrensEmergencyDepartment/guidelines/BCH_guidelines/1/index.html#11537))
